export class Product {
    id: number;
    title: string;
    description: string;
    icon: string;
}