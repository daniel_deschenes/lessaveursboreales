const AWS = require('aws-sdk');
const dynamodb = new AWS.DynamoDB({region: 'us-west-2', apiVersion: '2012-08-10'});

exports.handler = (event, context, callback) => {
    const params = {
        TableName: 'lsb-products'
    };
    dynamodb.scan(params, function(err, data) {
        if (err) {
            console.log(err);
            callback(err);
        } else {
            console.log(data);
            const items = data.Items.map((dataField) => {
                return {
                    id: dataField.Id.S, 
                    title: dataField.Title.S, 
                    description: dataField.Description.S
                };
            });
            callback(null, items);
        }
    });
};